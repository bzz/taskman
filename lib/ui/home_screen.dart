import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taskman/main.dart';
import 'package:taskman/controllers/todo_controller.dart';
import 'package:taskman/ui/todo_screen.dart';

class HomeScreen extends StatelessWidget {
  static const id = '/Home_screen';
  final TodoController c = Get.put(TodoController());

  HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(MyApp.title),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(
          Icons.add,
        ),
        onPressed: () => Get.to(() => TodoScreen(index: null)),
      ),
      body: Obx(
        () => ListView.builder(
          itemBuilder: (context, index) => Dismissible(
            key: UniqueKey(),
            background: Container(
              color: Colors.deepOrange,
              child: const Icon(
                Icons.delete,
                color: Colors.white,
              ),
            ),
            onDismissed: (_) {
              c.todos.removeAt(index);
              Get.snackbar('Remove', "Task removed",
                  snackPosition: SnackPosition.BOTTOM);
            },
            child: ListTile(
              title: Text(
                c.todos[index].text,
              ),
              trailing: IconButton(
                onPressed: () => Get.to(() => TodoScreen(index: index)),
                icon: const Icon(Icons.edit),
              ),
              leading: Checkbox(
                value: c.todos[index].done,
                onChanged: (neWvalue) {
                  var todo = c.todos[index];
                  todo.done = neWvalue!;
                  c.todos[index] = todo;
                },
              ),
            ),
          ),
          itemCount: c.todos.length,
        ),
      ),
    );
  }
}
