import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taskman/controllers/todo_controller.dart';
import 'package:taskman/models/todo.dart';

class TodoScreen extends StatelessWidget {
  static const id = '/Edit_screen';
  final int? index;
  final TodoController c = Get.find<TodoController>();

  TodoScreen({Key? key, @required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController textEditingController = index == null
        ? TextEditingController()
        : TextEditingController(text: c.todos[index!].text);
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(40),
        child: Column(
          children: [
            Expanded(
              child: TextField(
                decoration: const InputDecoration(
                  hintText: "Enter your task here",
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                ),
                style: const TextStyle(
                  fontSize: 25.0,
                ),
                keyboardType: TextInputType.multiline,
                maxLines: 10,
                autofocus: true,
                controller: textEditingController,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                // ignore: deprecated_member_use
                RaisedButton(
                  child: const Text('Cancel'),
                  color: Colors.red,
                  textColor: Colors.white,
                  onPressed: () {
                    Get.back();
                  },
                ),
                // ignore: deprecated_member_use
                RaisedButton(
                  child:
                      index == null ? const Text('Add') : const Text('Update'),
                  color: Colors.blue,
                  textColor: Colors.white,
                  onPressed: () {
                    if (index == null) {
                      if (textEditingController.text.isEmpty) {
                        return;
                      }
                      c.todos.add(
                        Todo(
                          text: textEditingController.text,
                        ),
                      );
                      Get.back();
                    } else {
                      var editing = c.todos[index!];
                      editing.text = textEditingController.text;
                      c.todos[index!] = editing;
                      Get.back();
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
