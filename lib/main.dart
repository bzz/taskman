import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:get/get.dart';
import 'package:taskman/ui/home_screen.dart';
import 'package:taskman/ui/todo_screen.dart';

void main() async {
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  static const String title = 'ToDo List';

  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: title,
      debugShowCheckedModeBanner: false,
      initialRoute: HomeScreen.id,
      getPages: [
        GetPage(name: HomeScreen.id, page: () => HomeScreen()),
        GetPage(name: TodoScreen.id, page: () => TodoScreen(index: null)),
      ],
    );
  }
}
